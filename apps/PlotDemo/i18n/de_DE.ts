<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.ui" line="14"/>
        <source>Plot Demo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="61"/>
        <source> Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="80"/>
        <source>Frequency</source>
        <translation type="unfinished">Frequenz</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="104"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="111"/>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="134"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="170"/>
        <source>Crosshair</source>
        <translation type="unfinished">Fadenkreuz</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="173"/>
        <source>Enable the crosshair cursor</source>
        <translation type="unfinished">Fadenkreuz aktivieren</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="182"/>
        <source>Snapshot</source>
        <translation type="unfinished">Schnappschuss</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="185"/>
        <source>Take a snapshot of the figure</source>
        <translation type="unfinished">Schnappschuss der Abbildung erstellen</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="194"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="197"/>
        <source>Export the current data</source>
        <translation type="unfinished">Datensatz exportieren</translation>
    </message>
</context>
<context>
    <name>MyMainWindow</name>
    <message>
        <location filename="PlotDemo.py" line="93"/>
        <source>Save Figure</source>
        <translation type="unfinished">Abbildung Speichern</translation>
    </message>
    <message>
        <location filename="PlotDemo.py" line="98"/>
        <source>Export Data</source>
        <translation type="unfinished">Daten exportieren</translation>
    </message>
</context>
</TS>
