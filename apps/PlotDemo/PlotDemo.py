import sys
from PySide import QtGui
from PySide.QtGui import *  # @UnusedWildImport
from PySide.QtCore import QTranslator, QLocale
from MainWindow import Ui_MainWindow
import numpy as np
import matplotlib
import icons_rc # @UnusedImport
import scipy.io
import h5py

# specify the use of PySide
matplotlib.rcParams['backend.qt4'] = "PySide"

# import the figure canvas for interfacing with the backend
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas

# import 3D plotting
from mpl_toolkits.mplot3d import Axes3D  # @UnusedImport
from matplotlib.figure import Figure

from AutoQObject import AutoQObject

m = AutoQObject(
    ('crosshair', bool),
    ('frequency', float),
    ('x', float),
    ('y', float),
    ('X', np.ndarray),
    name='Model'
)(frequency=1, x=0.1, y=0.2, X=np.array([1,2]))


class MyMainWindow(QtGui.QMainWindow):
    
    def __init__(self, parent=None):
        super(MyMainWindow, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.fig = Figure((5.0, 4.0), dpi=70, facecolor='white')

        self.canvas = FigureCanvas(self.fig)

        hbox = QtGui.QHBoxLayout()
        hbox.setContentsMargins(0, 0, 0, 0)
        hbox.addWidget(self.canvas)
        
        self.canvas.mpl_connect('button_press_event', self.onClick)

        self.ui.plotFrame.setLayout(hbox)
        
        # UI -> controller
        self.ui.actionSnapshot.triggered.connect(self.saveFigure)
        self.ui.actionExport.triggered.connect(self.exportData)
        
        # UI -> model
        self.ui.frequencySpinBox.valueChanged.connect(m._set_frequency)
        self.ui.actionCrosshair.toggled.connect(m._set_crosshair)
        self.ui.xSpinBox.valueChanged.connect(m._set_x)
        self.ui.ySpinBox.valueChanged.connect(m._set_y)
        
        # model -> UI
        m._nfy_x.connect(self.ui.xSpinBox.setValue)
        m._nfy_y.connect(self.ui.ySpinBox.setValue)
        
        # connect all model signals to plotSomething()
        for (key,value) in m.__dict__.items():
            if key.startswith('_nfy_'):
                value.connect(self.updatePlot)
                value.emit(getattr(m, key[5:]))
                
        self.updatePlot()
        
    def updatePlot(self):
        
        x = np.linspace(0, 2*np.pi, 100)
        y = np.sin(x * m.frequency)

        self.fig.clf()
        
        ax = self.fig.add_subplot(1,1,1)
        
        ax.plot(x, y)
        
        if m.crosshair:
            ax.axhline(m.y, color='k')
            ax.axvline(m.x, color='k')
        
        self.canvas.draw()
        
    def saveFigure(self):
        fname, _ = QFileDialog.getSaveFileName(self, self.tr('Save Figure'), 'figure', 'PDF (*.pdf);;PNG (*.png)')
        if fname:
            self.fig.savefig(fname)
    
    def exportData(self):
        fname, _ = QFileDialog.getSaveFileName(self, self.tr('Export Data'), 'sine', 'MAT (*.mat);; HDF5 (*.h5)')
        if fname:
            if fname.endswith('.mat'):
                mat = dict()
                
                x = np.linspace(0, 2*np.pi, 100)
                y = np.sin(x * m.frequency)
                
                mat['x'] = x
                mat['y'] = y
                
                scipy.io.savemat(fname, mat, format='4')
            else:
                f = h5py.File(fname, "w")
                f.create_dataset("mydataset", (100,), dtype='i')
                f.close()
        
    def onClick(self, event):
        if m.crosshair and event.xdata and event.ydata:
            m.x = event.xdata
            m.y = event.ydata

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    
    translator = QTranslator()
    translator.load('i18n/' + QLocale.system().name())
    app.installTranslator(translator)
    
    myapp = MyMainWindow()
    myapp.show()
    sys.exit(app.exec_())