'''
Various ways to populate arrays
'''

import numpy as np

# create an array from entered values
a = np.array([1, 2, 3, 5]) 

# create an array of linearly spaced sample points
b = np.linspace(0, 1, 6)

# concatenate two arrays
c = np.concatenate((a, b))
