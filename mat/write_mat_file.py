'''
Write a MAT file 
'''

# import the scipy.io and numpy modules
import scipy.io
import numpy as np

# create a dictionary for our data    
mat = dict()

# add a scalar 
mat['a'] = 1

# add a vector (1D)
mat['b'] = np.linspace(1,3,3)

# add a matrix (2D)
mat['c'] = np.reshape(np.linspace(1,6,6), (2,3))

# save the variables as a MAT v4 file
scipy.io.savemat('test.mat', mat, format='4')