'''
Read a MAT file
'''

from scipy.io import loadmat

# load the mat file
mat = loadmat('test.mat')
    
# we can access the values directly...
a = mat['a']
b = mat['b']
c = mat['c']


# ... or iterate over contents
for (name, value) in mat.items():
    # print name and value of the variable
    print '%s = %s' % (name, value)
