'''
This example shows how to use key word arguments
'''

# define a function that takes key-word arguments
def print_keyword_args(**kwargs):
    for key, value in kwargs.iteritems():
        print '%s = %s' % (key, value)

# call the function with to arguments
print_keyword_args(first_name='John', last_name='Doe')

# define a dictionary with the same arguments as above
args = {'first_name': 'John', 'last_name': 'Doe'}

# **args upacks the dictionary and yields the same result
print_keyword_args(**args)
