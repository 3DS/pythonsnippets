'''
This example shows how to use the datetime class to create timestamps
'''
from datetime import datetime

# create a timestamp from the current time and date
print datetime.now().strftime('%Y-%m-%d %H:%M:%S')

