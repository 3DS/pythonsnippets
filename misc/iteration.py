'''
Various ways to iterate over collections
'''

# create a list of names
names = ["Peter Lustig", "Hans Wurst", "John Doe"]

# iterate over the list
for name in names:
    print name

# iterate over list using indices
for i in range(len(names)):
    print '%s: %s' % (i, names[i])
    
# iterate over the array using enumerate
for i, name in enumerate(names):
    print '%s: %s' % (i, name)
    
# create a list of ages
ages = [65, 12, 50]

# this will give us a list of tuples [(name, age), ...]
people = zip(names, ages)

# iterate over the tuples
for person in people:
    print '%s is %s years old' % person
