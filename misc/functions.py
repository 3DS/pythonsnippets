'''
This example shows how to use functions
'''

# define a function that prints 'Hello Python!'
def say_hello():
    print 'Hello Python!'

# define another function that adds two numbers
def add_two_numbers(a, b):
    return a + b

# call the first function
say_hello()

# define the parameters
a = 1
b = 2

# call the second function
c = add_two_numbers(a, b)

# print the result
print '%d + %d = %d' % (a,b,c)