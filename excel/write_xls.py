'''
Write data to an Excel workbook

For an in-depth tutorial see http://www.simplistix.co.uk/presentations/python-excel.pdf
'''

# import the excel write module
import xlwt

book = xlwt.Workbook()

sheet1 = book.add_sheet('Sheet 1')

# write a string to A1
# first argument is the row index (starting with 0)
# second argument is the column index
# third argument is the value to write
sheet1.write(0, 0, 'Hello Python!')

# write a number to B1
sheet1.write(0, 1, 123)

# save the workbook
book.save('test.xls')
