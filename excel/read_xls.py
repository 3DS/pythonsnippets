'''
Read data from an Excel workbook

For an in-depth tutorial see http://www.simplistix.co.uk/presentations/python-excel.pdf
'''

# import the excel read module
import xlrd

# open the workbook
book = xlrd.open_workbook('test.xls')
        
sh = book.sheet_by_index(0)

# read the value from A1
# first argument is the row index (starting with 0),
# second argument is the column index
A1 = sh.cell_value(0, 0)

# read the value from B1
B1 = sh.cell_value(0, 1)

# print the values
print 'A1: %s' % A1
print 'B1: %s' % B1
    