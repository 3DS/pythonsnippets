'''
This example shows how to display the progress of a worker thread
and how to cancel it using a progress dialog
'''
import sys
from PySide.QtCore import QThread, Signal, Qt
from PySide.QtGui import QApplication, QProgressDialog

class WorkerThread(QThread):
    
    currentTask = Signal(str) 
    progress = Signal(int)

    def __init__(self, tasks=100, parent=None):
        QThread.__init__(self, parent)
        self.tasks = tasks
        self._cancel = False
        self.canceled = False
        
    def cancel(self):
        self._cancel = True
        
    def run(self):

        for i in range(self.tasks):
            
            if self._cancel:
                self.canceled = True
                return
            
            self.currentTask.emit('Processing task no. {0}'.format(i))
            self.progress.emit(i)
            self.msleep(100)
            
if __name__ == "__main__":
    app = QApplication(sys.argv)

    workerThread = WorkerThread(tasks=200)
    
    progressDialog = QProgressDialog(maximum=workerThread.tasks, minimumDuration=100)
    progressDialog.setWindowTitle('Importing Time Series')
    progressDialog.setLabelText('Importing time series...')
    progressDialog.setWindowModality(Qt.WindowModal)
    progressDialog.canceled.connect(workerThread.cancel)
    progressDialog.show()

    workerThread.currentTask.connect(progressDialog.setLabelText)
    workerThread.progress.connect(progressDialog.setValue)
    workerThread.finished.connect(progressDialog.close)
    
    workerThread.start()
    
    app.exec_()
